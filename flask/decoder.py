#3. decoder

from PIL import Image

def decode(stego_image):
    # Open the stego image
    stego_image = Image.open(stego_image).convert('L')
    width, height = stego_image.size

    # Extract the LSBs from the pixel values and convert to binary
    data_bin = ''
    for y in range(height):
        for x in range(width):
            pixel_bin = '{:08b}'.format(stego_image.getpixel((x, y)))
            data_bin += pixel_bin[-1]

    # Extract the length of the data from the beginning of the binary string
    data_len = int(data_bin[:8], 2)
    data_bin = data_bin[8:8+data_len*8]

    # Convert the binary string back to bytes
    data = bytes([int(data_bin[i:i+8], 2) for i in range(0, len(data_bin), 8)])

    # Write the extracted data to a file

    return data

    print("Data extracted successfully!")
